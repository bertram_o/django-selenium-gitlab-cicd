from django.test import LiveServerTestCase
from selenium import webdriver
from time import sleep
from selenium.webdriver.firefox.options import Options


class HelloWorldTest(LiveServerTestCase):
    def setUp(self) -> None:
        opts = Options()
        opts.headless = True
        self.firefox_driver = webdriver.Firefox(options=opts)

    def test_hello_world(self):
        self.firefox_driver.get(self.live_server_url)
        sleep(0.5)
        header_div = self.firefox_driver.find_element_by_id('id_header')
        self.assertEqual(header_div.text, "Hello Selenium!")

    def tearDown(self) -> None:
        self.firefox_driver.quit()
