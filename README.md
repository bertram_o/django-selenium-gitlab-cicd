# Django Selenium GitLab CICD

An example for GitLab CI/CD to execute functional tests with
selenium (geckodriver/firefox) for a Django app.
* Dockerfile-CICD based on official Python image plus Firefox and Geckodriver
* CI bakes Dockerfile-CICD into repo registry ("buildimage"), gets rebuilt 
  if changes in Dockerfile-CICD for pushes on master branch (questionable 
  strategy)
* functional tests in CI pipeline use this image as base, but app 
  requirements are installed ad-hoc